\documentclass{beamer}
%\documentclass[handout]{beamer}
  \usetheme{Berkeley}
  \usecolortheme{crane}
  \usefonttheme{professionalfonts}
  \usefonttheme{serif}
\usepackage{polyglossia}
  \setmainlanguage[babelshorthands=true]{italian}
\usepackage{fontspec}
  \setmainfont{Liberation Serif}
  \setsansfont{Liberation Sans}
  \setmonofont{Liberation Mono}
  \frenchspacing
\usepackage{caption}
  \captionsetup[figure]{labelformat=empty,singlelinecheck=off,
  justification=centering}
  \captionsetup[table]{labelformat=empty,singlelinecheck=off,
  justification=centering}
\usepackage{graphicx}
  \graphicspath{{res/}}
\usepackage{booktabs}

\AtBeginSection[]
{\begin{frame}
  \frametitle{Table of Contents}
  \tableofcontents[currentsection]
\end{frame}}

\title[RANKS]{A Fast Ranking Algorithm for Predicting Gene
Functions in Biomolecular Networks}
\subtitle{Bioinformatica}
\author[Riccardo Macoratti]{Riccardo Macoratti (matr. 844553)}
\date{}

\begin{document}
 
\frame{\titlepage}

\begin{frame}
  \frametitle{Indice}
  \tableofcontents
\end{frame}

\section{Introduzione}
\begin{frame}
  \frametitle{Problema}
  \textbf{Problema:} predire la funzione biologica di un gene (\textit{Gene
  Function Prediction} o GFP) \\
  \begin{itemize}
    \item i dati sui geni sono disponibili sotto forma di \textit{grafo}
    \item le caratteristiche distintive di un gene sono molte, perciò la GFP è
      un compito computazionalmente impegnativo
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Approcci già disponibili}
  Sono stati tentati diversi approcci in passato, tra cui: \\
  \begin{itemize}
    \item algoritmi in grado di quantificare le similarità tra le sequenze di
      proteine
    \item algoritmi di apprendimento automatico per inferire regole di
      annotazione basate su vettori di caratteristiche
    \item metodi basati su reti che non utilizzano un modello predittivo
      globale
      \begin{itemize}
        \item \textit{guilt-by-association} (GBA)
        \item integrazione di strategie di apprendimento locali tramite
          combinazioni pesate
        \item reti di Hopfield
        \item analisi di proteine omologhe in altre specie
        \item propagazione di etichette basata su reti di Markov
        \item campi aleatori gaussiani
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Perché un nuovo approccio?}
  Gli algoritmi basati su reti hanno dei \textbf{limiti}: \\
  \begin{itemize}
    \item basse prestazioni nella predizione
    \item scalabilità (dovuta alle nuove tecnologie ad alto \textit{throughput})
  \end{itemize}
  Il nuovo approccio è un metodo \textbf{trasduttivo semi-supervisionato}, che
  presenta due aspetti:
  \begin{itemize}
    \item una generalizzazione del concetto di distanza media
    \item una strategia di apprendimento globale attraverso \textit{kernel} che
      sfruttino la topologia della rete
  \end{itemize}
  Inoltre il metodo proposto è \textbf{lineare} nel numero di geni e non
  richiede l'apprendimento di nessun modello.
\end{frame}

\section{Funzioni di Score Kernelizzate}

\subsection{Funzioni di Score}
\begin{frame}
  \frametitle{Preambolo}
  Si definiscono:
  \begin{itemize}
    \item $G = (V, E)$ un grafo pesato non orientato
      \begin{itemize}
        \item $V$ è l'insieme dei geni
        \item $E$ rappresenta la correlazione funzionale
      \end{itemize}
    \item $W = [w_{ij}]$ la matrice di adiacenza di $G$
    \item $V_C \subset V$ il set di etichette positive per la categoria $C$
     
    \item un insieme $X$ di \textit{feature} $x_i \in X$
    \item una funzione \textit{kernel} $K: X \times X \rightarrow \mathbb{R}$,
      tale che $< \phi(x), \phi(x) >_{\mathcal{H}} = K(x, x)$
    \item una funzione di conversione allo spazio di Hilbert $\phi: X
      \rightarrow \mathcal{H}$
  \end{itemize}
  L'obiettivo è quello di definire una funzione di score $S: V \rightarrow
  \mathbb{R}^+$ basata su una misura di distanza $D(i, V_C, X)$ nello spazio di
  Hilbert.
\end{frame}

\begin{frame}
  \frametitle{Average Score Function}
  $D_{AV}(i, V_C)$ è la distanza media tra il nodo $i$ e i nodi positivi alla 
  \textit{feature} $C$.
  \[
    D_{AV}(i, V_C) = \lVert \phi(x_i) - \frac{1}{\lvert V_C \rvert} \sum_{j \in
    V_C} \phi(x_j) \rVert^2
  \] \\
  Sviluppando e invertendo il segno per avere una misura di \textbf{similarità},
  ottengo:
  \[
    S_{AV}(i, V_C) = \frac{2}{\lvert V_C \rvert} \bigg( \sum_{j \in V_C} K(x_i,
    x_j) \bigg) - K(x_i, x_i)
  \]
\end{frame}

\begin{frame}
  \frametitle{Nearest-Neighbours Score Function}
  $D_{NN}(i, V_C)$ è la minima distanza tra il nodo $i$ e i nodi positivi alla
  \textit{feature} $C$.
  \[
    D_{NN}(i, V_C) = \min_{j \in V_C} \lVert \phi(x_i) - \phi(x_j) \rVert^2
  \] \\
  Sviluppando e invertendo il segno per avere una misura di \textbf{similarità}
  (supponendo che $K(x_j, x_j)$ sia uguale $\forall j \in V$):
  \[
    D_{NN}(i, V_C) = - \min_{j \in V_C} -2K (x_i, x_j) = 2 \max_{j \in V_C}
    K(x_i, x_i)
  \]
\end{frame}

\begin{frame}
  \frametitle{K-Nearest-Neighbours Score Function}
  $D_{kNN}(i, V_C)$ è somma delle distanze tra il nodo $i$ e i primi $k$ nodi
  positivi alla \textit{feature} $C$.
  \[
    D_{kNN}(i, V_C) = \sum_{j \in I_k(i)} \lVert \phi(x_i) - \phi(x_j) \rVert^2
  \] Dove $I_k(i) = \{j \in V_C \mid j$ è posizionato nei primi $k$ in $V_C\}$
  \\
  Sviluppando e invertendo il segno per avere una misura di \textbf{similarità}
  (supponendo che $K(x_j, x_j)$ sia uguale $\forall j \in V$):
  \[
    D_{kNN}(i, V_C) = 2 \sum_{j \in I_k(i)} K(x_i, x_j)
  \]
\end{frame}

\begin{frame}
  \frametitle{Riflessione sulle prestazioni}
  Tutte le nozioni di similarità dipendono solo dal nodo in esame e dai suoi
  \textbf{vicini}. \\~\\ 
  Una volta che la matrice del \textit{kernel} è stata calcolata, nel peggior
  caso gli algoritmi hanno complessità $\mathcal{O}(|V| \cdot |V_C|)$. \\
  Per le proprietà delle ontologie di geni, dove le \textit{feature}
  positive sono in numero assai minore di quelle negative e quindi $|V_C| \ll
  |V|$, la complessità è assimilabile a quella lineare.
\end{frame}

\subsection{\textit{Kernel} per passeggiate aleatorie}
\begin{frame}
  \frametitle{\textit{Kernel}}
  \begin{block}{\textit{Kernel}}
    Un \textit{kernel} è una funzione simmetrica e può essere rappresentato
    tramite prodotto interno in un opportuno spazio di Hilbert.
  \end{block}

  La rappresentazione dei dati tramite \textit{kernel} apporta dei vantaggi:
  \begin{itemize}
    \item dà una misura della similarità
    \item non dipende dalla natura dei dati
    \item rende la comparazione di oggetti è più semplice (rispetto alla
      rappresentazione esplicita)
  \end{itemize}
  Se un algoritmo sfrutta il prodotto interno tra vettori può essere eseguito
  nello spazio di Hilbert rimpiazzando i prodotti con valutazioni di
  \textit{kernel}. Il processo è detto \textbf{kernelizzazione}.
\end{frame}

\begin{frame}
  \frametitle{Quale \textit{kernel} scegliere}
  Dato il tipo di dati che stiamo analizzando ha senso sostituire la funzione
  generica $K(\cdot, \cdot)$ con un \textit{kernel} orientato alle reti.
  \\~\\
  Il kernel adottato è quello per \textbf{passeggiate casuali su grafo}, poiché
  permette di sfruttare a pieno la topologia della rete.

  Nello specifico si usa una variante del \textit{kernel} per
  \textbf{passeggiate casuali Markoviane}.
\end{frame}

\begin{frame}
  \frametitle{\textit{Kernel} per passeggiate aleatorie a $q$ passi}
  Posti $\tilde{L}$ la matrice laplaciana normalizzata, $W$ la matrice di
  adiacenza, $I$ la matrice identica e $D$ la matrice diagonale, il
  \textit{kernel} per passeggiate aleatorie è
  \[
    K_{rw} = (aI - \tilde{L})
  \] di conseguenza la versione a $q \geq 1$ passi è
  \[
    K_{rw}^q = {(aI - \tilde{L})}^q
  \] \\
  La distribuzione stazionaria del kernel si trova determinando l'autovalore
  dominante di
  \[
    \alpha I + (1 - \alpha) D^{-1} W
  \]
\end{frame}

\begin{frame}
  \frametitle{Vantaggi del \textit{random walk kernel} a $q$ passi}
  A differenza della passeggiata aleatoria classica, questa variante permette di
  lavorare con grafi non orientati e quindi matrici di adiacenza simmetriche.
  \\~\\
  Inoltre è possibile implementare il \textit{$q$-step random walk kernel}
  ricorsivamente e incrementalmente utilizzando il metodo della potenza:
  \[
    K_{rw}^q = K_{rw}^{q-1}K_{rw}  
  \]
\end{frame}

\section{Test del metodo}

\subsection{Dati utilizzati}
\begin{frame}
  \frametitle{Dati utilizzati}
  È stato scelto di predire le classi del \textbf{lievito} rispetto
  all'ontologia FunCat. \\
  
  Sono stati utilizzati \textbf{6} insiemi di dati riguardanti
  \begin{itemize}
    \item interazioni proteina-proteina
    \item dominio delle proteine
    \item espressione dei geni
    \item similarità delle sequenze
  \end{itemize}
  e sono stati scelti solo i dati annotati, con almeno 20 esempi positivi. \\

  La selezione ha portato ad avere \textbf{1901} geni del lievito annotati a
  \textbf{168} classi, distribuite su 16 alberi e 5 livelli gerarchici. \\~\\

  Le matrici dei \textit{kernel} sono state calcolate usando un \textit{kernel}
  lineare e ottenendo 6 matrici quadrate e simmetriche, successivamente
  integrate sommando le matrici stesse.
\end{frame}

\begin{frame}
  \frametitle{Grafo risultante}
  Rimuovendo tutti i nodi isolati e impostando delle soglie adeguate per i pesi
  degli archi è stato ottenuto un grafo con \textbf{1901} nodi e \textbf{489338}
  archi, con una densità pari a \textbf{0.1354}.
\end{frame}

\subsection{Metodi in comparazione}
\begin{frame}
  \frametitle{Metodi scelti per la comparazione}
  Sono stati scelti i seguenti metodi per comparare quello proposto:
  \begin{itemize}
    \item \textit{GeneMANIA}, basato su campi aleatori gaussiani
    \item la passeggiata aleatoria classica
    \item la passeggiata aleatoria classica a 2 passi
    \item la passeggiata aleatoria classica con tassazione
    \item una versione probabilistica di \textit{Support Vector Machine}
    \item una versione semplice di metodo GBA
  \end{itemize}
  Le prestazioni dei metodi, dato lo sbilanciamento di FunCat, sono state
  valutate utilizzando l'area sotto la curva (AUC) e la precisione a diversi
  livelli di recupero.
\end{frame}

\section{Risultati}

\subsection{Comparazione delle funzioni di score}
\begin{frame}
  \frametitle{Impatto del parametro $k$ in $S_{kNN}$}
  Si è valutato l'impatto del parametro $k$ nella funzione di score a $k$
  vicini. \\
  Con $3 \leq k \leq 23$ e valutando la precisione a diversi livelli di recall,
  i migliori risultati si sono raggiunti in $19 \leq k \leq 23$.
  \begin{figure}
    \includegraphics[width=.7\textwidth]{fig1}
    \centering
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Impatto del parametro $q$ nella passeggiata aleatoria}
  Si inoltre è valutato l'impatto del parametro $q$ nel \textit{kernel} per la
  passeggiata aleatoria a $q$ passi. \\
  Per ogni funzione di score è stata simulata la passeggiata aleatoria a 1, 2 e
  3 passi. $S_{kNN}$ e $S_{AV}$ superano $S_{NN}$ indipendentemente da $q$,
  mentre $S_{kNN}$ supera $S_{AV}$ nella passeggiata a 2 o 3 passi. \\
  Nonostante ciò, la passeggiata a singolo passo da i migliori risultati.
  \begin{figure}
    \includegraphics[width=.55\textwidth]{fig2}
    \centering
  \end{figure}
\end{frame}

\subsection{Comparazione tra i metodi di ordinamento allo stato dell'arte}
\begin{frame}
  \frametitle{Comparazione delle predizioni}
  \begin{figure}
    \includegraphics[height=.9\textheight]{fig3}
    \centering
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Comparazione dell'area sotto la curva}
  \begin{table}[]
    \centering
    \begin{tabular}{@{}lllll@{}}
      \cmidrule(r){1-4}
      $S_{kNN}$ & $S_{AV}$ & $S_{NN}$ & GM &  \\ \cmidrule(r){1-4}
      0.8840 & 0.8782 & 0.8649 & 0.8614 &  \\ \midrule
      RW & RW R & RW 2 passi & SVM & \multicolumn{1}{l}{GBA} \\ \midrule
      0.5118 & 0.8383 & 0.7660 & 0.7770 & 0.8014 \\ \bottomrule
    \end{tabular}
    \caption{Valori dell'area sotto la curva, aggregati tramite la media tra le
    varie matrici dei \textit{kernel}.}
  \end{table}
  \begin{block}{\textit{Area Under the Curve} (AUC)}
    È la probabilità che un classificatore ordini un risultato positivo scelto a
    caso prima di un risultato negativo scelto a caso e dà un'idea
    dell'accuratezza.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Comparazione del tempo empirico}
  \begin{table}[]
    \centering
    \begin{tabular}{@{}llllll@{}}
      \cmidrule(lr){2-5}
      & $S_{kNN}$ & $S_{AV}$ & $S_{NN}$ & GM &  \\ \cmidrule(r){1-5}
      \multicolumn{1}{l|}{tempo} & 10 & 7 & 7 & 118 &  \\ \midrule
      & RW & RW R & RW 2 passi & SVM & \multicolumn{1}{l}{GBA} \\ \midrule
      \multicolumn{1}{l|}{tempo} & 3297 & 435 & 85 & 21805 & 6 \\ \bottomrule
    \end{tabular}
    \caption{Tempo empirico di calcolo (valutato per una \textit{5-fold cross
    validation}) espresso in secondi.}
  \end{table}
\end{frame}

\section*{Bibliografia}
\begin{frame}
  \frametitle{Bibliografia}    
  \begin{thebibliography} 
      \beamertemplatearticlebibitems{}
    \bibitem{Valentini2012}
      Matteo Re, Marco Mesiti and Giorgio Valentini
      \newblock{} ``A Fast Ranking Algorithm for Predicting Gene Functions in
      Biomolecular Network''
      \newblock{} IEEE ACM Transactions on Computational Biology and
      Bioinformatics 9(6) pp. 1812--1818, 2012.
  \end{thebibliography}
\end{frame}

\end{document}
